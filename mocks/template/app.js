'use strict';

/* eslint-disable no-console */

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const PORT = parseInt(process.env.PORT || 3000);
// should follow the pattern: x-mock-{service_name}
const MOCK_HEADER_STR = 'x-mock-template';

//--- main

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/api/hello', (req, res, next) => {
  const mockHeader = req.get(MOCK_HEADER_STR);
  res.set('X-Example-Header', 'hello');
  switch (mockHeader) {
    case 'give-me-a-500-error': {
      return res.status(500).end();
    }

    default: {
      return res.json({ status: 'ok' });
    }
  }
});

app.listen(PORT, '0.0.0.0', err => {
  if (err) console.log(err);

  console.log(`> server listening on http://0.0.0.0:${PORT}`);
});
