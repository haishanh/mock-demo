'use strict';

/* eslint-disable no-console */

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const PORT = parseInt(process.env.PORT || 3000);
// should follow the pattern: x-mock-{service_name}
const MOCK_HEADER_STR = 'x-mock-hacker_news';

//--- main

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const payloadGetItemDefault = {
  by: 'dhouston',
  descendants: 71,
  id: 8863,
  kids: [8952, 8870, 8876],
  score: 104,
  time: 1175714200,
  title: 'My YC app: Dropbox - Throw away your USB drive',
  type: 'story',
  url: 'http://www.getdropbox.com/u/2/screencast.html'
};

app.get('/v0/item/:item', (req, res, next) => {
  const mockHeader = req.get(MOCK_HEADER_STR);
  switch (mockHeader) {
    case '500': {
      return res.status(500).json({
        errcode: '10050',
        errmsg: 'Unknown Server Internal Error'
      });
    }

    default: {
      return res.json(payloadGetItemDefault);
    }
  }
});

app.listen(PORT, '0.0.0.0', err => {
  if (err) console.log(err);

  console.log(`> server listening on http://0.0.0.0:${PORT}`);
});
